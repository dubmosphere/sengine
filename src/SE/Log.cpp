// Class includes
#include <SE/Log.hpp>

// STL includes
#include <string>
#include <iostream>

namespace se
{
    std::ofstream Log::logStream("log/SE.log", std::ios::app);

    Log::~Log()
    {
        logStream.close();
    }

    bool Log::write(LogType type, std::string message)
    {
        if(logStream.is_open()) {
            std::string strType;

            switch(type) {
                case LogType::ERROR:
                    strType = "ERROR";
                    break;
                case LogType::DEBUG:
                    strType = "DEBUG";
                    break;
                case LogType::INFO:
                default:
                    strType = "INFO";
                    break;
            }

            logStream << strType << ": " << message << std::endl;

            return true;
        }

        return false;
    }
}
