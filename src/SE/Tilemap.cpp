// Class includes
#include <SE/Tilemap.hpp>

// STL includes
#include <fstream>

namespace se {
    Tilemap::Tilemap() {

    }

    bool Tilemap::load(sf::String tileSet, sf::Vector2i mapSize, sf::Vector2i tileSize, int *tiles) {
        if(!this->tileSet.loadFromFile(tileSet))
            return false;

        this->mapSize = mapSize;
        this->tileSize = tileSize;

        this->vertices.setPrimitiveType(sf::Quads);
        this->vertices.resize(this->mapSize.x * this->mapSize.y * 4);

        for(int i = 0; i < this->mapSize.y; ++i) {
            std::vector<Tile> rows;

            for(int j = 0; j < this->mapSize.x; ++j) {
                Tile tile;
                tile.tileNum = tiles[j + i * this->mapSize.x];

                tile.pos = sf::Vector2f(j * this->tileSize.x, i * this->tileSize.y);

                if(tile.tileNum > 0) {
                    int tu = (tile.tileNum - 1) % (this->tileSet.getSize().x / this->tileSize.x);
                    int tv = (tile.tileNum - 1) / (this->tileSet.getSize().x / this->tileSize.x);

                    sf::Vertex *quad = &this->vertices[(j + i * this->mapSize.x) * 4];

                    quad[0].position = sf::Vector2f(j * this->tileSize.x, i * this->tileSize.y);
                    quad[1].position = sf::Vector2f((j + 1) * this->tileSize.x, i * this->tileSize.y);
                    quad[2].position = sf::Vector2f((j + 1) * this->tileSize.x, (i + 1)* this->tileSize.y);
                    quad[3].position = sf::Vector2f(j * this->tileSize.x, (i + 1) * this->tileSize.y);

                    quad[0].texCoords = sf::Vector2f(tu * this->tileSize.x, tv * this->tileSize.y);
                    quad[1].texCoords = sf::Vector2f((tu + 1) * this->tileSize.x, tv * this->tileSize.y);
                    quad[2].texCoords = sf::Vector2f((tu + 1) * this->tileSize.x, (tv + 1)* this->tileSize.y);
                    quad[3].texCoords = sf::Vector2f(tu * this->tileSize.x, (tv + 1) * this->tileSize.y);
                }

                rows.push_back(tile);
            }

            this->tiles.push_back(rows);
        }

        return true;
    }

    bool Tilemap::load(std::string fileMap) {
        std::ifstream in(fileMap.c_str(), std::ios::in);

        if(in.is_open()) {
            char buffer[255];

            in.getline(buffer, 255, '\n');
            if(!this->tileSet.loadFromFile(buffer))
                return false;

            in.getline(buffer, 8, '\n');
            this->mapSize.x = atoi(buffer);

            in.getline(buffer, 8, '\n');
            this->mapSize.y = atoi(buffer);

            in.getline(buffer, 8, '\n');
            this->tileSize.x = atoi(buffer);

            in.getline(buffer, 8, '\n');
            this->tileSize.y = atoi(buffer);

            this->vertices.setPrimitiveType(sf::Quads);
            this->vertices.resize(this->mapSize.x * this->mapSize.y * 4);

            for(int i = 0; i < this->mapSize.y; ++i) {
                std::vector<Tile> rows;

                for(int j = 0; j < this->mapSize.x; ++j) {
                    Tile tile;

                    in.getline(buffer, 8, ',');
                    tile.tileNum = atoi(buffer);

                    in.getline(buffer, 8, '|');
                    tile.solid = (atoi(buffer) == 1) ? true : false;

                    if(tile.tileNum > 0) {
                        int tu = (tile.tileNum - 1) % (this->tileSet.getSize().x / this->tileSize.x);
                        int tv = (tile.tileNum - 1) / (this->tileSet.getSize().x / this->tileSize.x);

                        sf::Vertex *quad = &this->vertices[(j + i * this->mapSize.x) * 4];

                        quad[0].position = sf::Vector2f(j * this->tileSize.x, i * this->tileSize.y);
                        quad[1].position = sf::Vector2f((j + 1) * this->tileSize.x, i * this->tileSize.y);
                        quad[2].position = sf::Vector2f((j + 1) * this->tileSize.x, (i + 1) * this->tileSize.y);
                        quad[3].position = sf::Vector2f(j * this->tileSize.x, (i + 1) * this->tileSize.y);

                        quad[0].texCoords = sf::Vector2f(tu * this->tileSize.x, tv * this->tileSize.y);
                        quad[1].texCoords = sf::Vector2f((tu + 1) * this->tileSize.x, tv * this->tileSize.y);
                        quad[2].texCoords = sf::Vector2f((tu + 1) * this->tileSize.x, (tv + 1) * this->tileSize.y);
                        quad[3].texCoords = sf::Vector2f(tu * this->tileSize.x, (tv + 1) * this->tileSize.y);
                    }

                    rows.push_back(tile);
                }

                this->tiles.push_back(rows);
            }

            in.close();
            return true;
        }

        return false;
    }

    sf::Vector2i Tilemap::getMapSize() {
        return this->mapSize;
    }

    sf::Vector2i Tilemap::getTileSize() {
        return this->tileSize;
    }

    Tile Tilemap::getTile(int x, int y) {
        return this->tiles.at(y).at(x);
    }

    void Tilemap::draw(sf::RenderTarget &target, sf::RenderStates states) const {
        states.transform *= this->getTransform();

        states.texture = &this->tileSet;

        target.draw(this->vertices, states);
    }
}
