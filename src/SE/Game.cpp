// Class includes
#include <SE/Game.hpp>

// SE includes
#include <SE/Log.hpp>

//SFML includes
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

// STL includes
#include <iostream>

// Own includes
#include <SE/GameState.hpp>

namespace se
{
    Game::Game() :
    FPS(500.f),
    TPF(sf::seconds(1.f / 60.f))
    {
        Log::write(Log::INFO, "se::Game constructed");
    }

    Game::~Game()
    {
        while(!gameStates.empty())
            popState();

        Log::write(Log::INFO, "se::Game destructed");
    }

    void Game::createWindow(sf::VideoMode mode, const sf::String& title, sf::Uint32 style, const sf::ContextSettings& settings)
    {
        window.create(mode, title, style, settings);
        Log::write(Log::INFO, "se::Game window created");
    }

    sf::RenderWindow &Game::getWindow()
    {
        return window;
    }

    int Game::run()
    {
        Log::write(Log::INFO, "se::Game ran");
        gameLoop();
        Log::write(Log::INFO, "se::Game exiting");

        return 0;
    }

    void Game::pushState(GameState* gameState)
    {
        gameStates.push(gameState);
    }

    void Game::popState()
    {
        gameStates.top()->deinit();
        delete gameStates.top();
        gameStates.pop();
    }

    void Game::changeState(GameState* gameState)
    {
        if(!gameStates.empty())
            popState();

        pushState(gameState);
    }

    GameState *Game::peekState()
    {
        if(gameStates.empty())
            return NULL;

        return gameStates.top();
    }

    void Game::updateLoop(sf::Time &accumulator, sf::Clock &clock) {
        while(accumulator >= Game::TPF) {
            sf::Event event;

            while(window.pollEvent(event))
            {
                if(peekState() != NULL)
                    peekState()->processEvents(event);

                switch(event.type)
                {
                    case sf::Event::Closed:
                        window.close();
                        break;
                    default:
                        break;
                }
            }

            if(peekState() != NULL)
                peekState()->updateFixed();

            accumulator -= Game::TPF;
        }

        if(peekState() != NULL)
            peekState()->updateUnfixed();
    }

    void Game::gameLoop()
    {
        sf::Time accumulator = sf::Time::Zero;
        sf::Clock clock;

        Log::write(Log::INFO, "se::Game gameloop started");

        while(window.isOpen()) {
            updateLoop(accumulator, clock);

            window.clear();

            if(peekState() != NULL)
                peekState()->render();

            window.display();

            accumulator += clock.restart();
        }

        Log::write(Log::INFO, "se::Game gameloop ended");
    }
}
