// Class includes
#include <SE/GameState.hpp>

namespace se
{
    GameState::~GameState() {}

    void GameState::init() {}

    void GameState::deinit() {}

    void GameState::processEvents(sf::Event &event) {}

    void GameState::updateFixed() {}

    void GameState::updateUnfixed() {}

    void GameState::render() {}
}
