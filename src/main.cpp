// SE includes
#include <SE/Log.hpp>
#include <SE/Game.hpp>
#include <SE/GameState.hpp>

// SFML includes
#include <SFML/Graphics/RectangleShape.hpp>

namespace GameStates {
    enum ID {
        GAMESTATE
    };
}

class GameState : public se::GameState {
    public:
        GameState(se::Game &game) :
        game(game),
        rect(sf::Vector2f(100.f, 100.f))
        {
            init();
        }

        void init() {

        }

        void deinit() {

        }

        void processEvents(sf::Event &event) {

        }

        void updateFixed() {

        }

        void updateUnfixed() {

        }

        void render() {
            game.getWindow().draw(rect);
        }

    private:
        se::Game &game;

        sf::RectangleShape rect;
};

int main(int argc, char **argv) {
    se::Game game;

    se::Log::write(se::Log::INFO, "Testlog");

    game.createWindow(sf::VideoMode(800, 600, 32), "EngineTest");
    game.getWindow().setFramerateLimit(500);

    game.pushState(new GameState(game));

    return game.run();
}
