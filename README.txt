DISCONTINUED

Hello,

this is a little game framework for SFML. All you need is
a static SFML 2.2 and Codeblocks 13.12 with GCC 4.9 on Linux and MinGW / MinGW-w64 4.9 on Windows.
(Maybe it would work in VS too, i did not test it)

The documentation is not finished yet, i am expanding it from time to time. You can compile the docs with doxygen.

I WILL NO LONGER WORK ON THIS PROJECT BECAUSE IT IS A LITTLE BIT MESSED UP AND I WOULD DO MANY THINGS DIFFERENT NOW,
I KEEP THE SOURCE CODE ON BECAUSE MAY SOMEONE FINDS SOMETHING INTERESTING IN IT.

BUILD INSTRUCTIONS:
-Open the codeblocks project
-Make sure you have all the libraries needed in the setup for your platform
-If you have the libraries installed, check the search directories of the project setup you want to use
-Press on build

USAGE (Codeblocks):
-To use SE you just need to put the static library file into your library folder
-You will need the include folder too, so put it somewhere you think you need
-Linux: For example /usr/lib and the include folder to /usr/include
-Windows: Add the path of the lib folder and include folder to your search directories in codeblocks

Have fun
Dubmosphere