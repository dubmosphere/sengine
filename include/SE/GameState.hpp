#ifndef SE_GAMESTATE_HPP
#define SE_GAMESTATE_HPP

namespace sf
{
    class Event;
}

namespace se
{
    /** \brief Interface class for making gamestates
     *
     * With this class you can make gamestates.\n
     * You have to implement  every method of this class for\n
     * creating a working gamestate. It's recommended to give the\n
     * subclasses of this class a reference to their parent game.
     */
    class GameState
    {
        public:
            /** \brief Virtual destructor
             *
             * Has no need to be overwritten
             */
            virtual ~GameState() = 0;

            /** \brief Initialize gamestate
             *
             * Virtual method has for initializing a gamestate,\n
             * has to be implemented in every gamestate.
             */
            virtual void init() = 0;

            /** \brief Deinitialize gamestate
             *
             * Virtual method has for deinitializing a gamestate,\n
             * has to be implemented in every gamestate.
             */
            virtual void deinit() = 0;

            /** \brief Process events in gamestate
             *
             * Virtual method has for processing events of a gamestate,\n
             * has to be implemented in every gamestate.
             *
             * \param event Reference of the event to process
             */
            virtual void processEvents(sf::Event &event) = 0;

            /** \brief Update gamestate logic
             *
             * Virtual method has for updating the gamestate logic,\n
             * has to be implemented in every gamestate.
             */
            virtual void updateFixed() = 0;

            virtual void updateUnfixed() = 0;

            /** \brief Render gamestate
             *
             * Virtual method has for rendering the gamestate,\n
             * has to be implemented in every gamestate.
             */
            virtual void render() = 0;
    };
}

#endif // SE_GAMESTATE_HPP
