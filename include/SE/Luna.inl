template <class T>
void Luna<T>::reg(lua_State *L) {
    lua_pushcfunction(L, &Luna<T>::constructor);
    lua_setglobal(L, T::className);

    luaL_newmetatable(L, T::className);
    lua_pushstring(L, "__gc");
    lua_pushcfunction(L, &Luna<T>::destructor);
    lua_settable(L, -3);
}

template <class T>
int Luna<T>::constructor(lua_State *L) {
    T *obj = new T(L);

    lua_newtable(L);
    lua_pushnumber(L, 0);
    T **pObj = static_cast<T**>(lua_newuserdata(L, sizeof(T*)));
    *pObj = obj;
    luaL_getmetatable(L, T::className);
    lua_setmetatable(L, -2);
    lua_settable(L, -3);

    for(int i = 0; T::reg[i].name; i++) {
        lua_pushstring(L, T::reg[i].name);
        lua_pushnumber(L, i);
        lua_pushcclosure(L, &Luna<T>::thunk, 1);
        lua_settable(L, -3);
    }

    return 1;
}

template <class T>
int Luna<T>::thunk(lua_State *L) {
    int i = static_cast<int>(lua_tonumber(L, lua_upvalueindex(1)));
    lua_pushnumber(L, 0);
    lua_gettable(L, 1);
    T **obj = static_cast<T**>(luaL_checkudata(L, -1, T::className));
    lua_remove(L, -1);
    return ((*obj)->*(T::reg[i].mfunc))(L);
}

template <class T>
int Luna<T>::destructor(lua_State *L) {
    T **obj = static_cast<T**>(luaL_checkudata(L, -1, T::className));
    delete (*obj);
    return 0;
}
