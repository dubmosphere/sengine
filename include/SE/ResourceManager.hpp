#ifndef SE_RECOURCEMANAGER_HPP
#define SE_RECOURCEMANAGER_HPP

// STL includes
#include <map>

namespace se
{
    /** \brief Manage the resources
     *
     * This class is for holding resources of any type SFML supports.\n
     * It makes it simple to manage the resources and only needs one class\n
     * for every resource type
     *
     * Since it is a template class, you have to give it a type.\n
     * This class needs a type (prefer enums) to identify the resources\n
     * and a resource type.
     */
    template <typename Resource, typename Identifier>
    class ResourceManager
    {
        public:
            /** \brief Destructor
             *
             * This is the basic destructor.\n
             * Don't override it, it will delete all pointers.
             */
            ~ResourceManager();

            /** \brief Load a resource into the manager
             *
             * Give the resource an ID and enter the filename to\n
             * load it into the manager.
             *
             * \param id Resource ID
             * \param filename The filename of the resource
             * \return A boolean if loading was successful
             */
            bool load(Identifier id, const std::string &filename);

            /** \brief Load a resource into the manager with 2 parameters
             *
             * Give the resource an ID and enter the filename to\n
             * load it into the manager. If the resource has a constructor with 2 parameters,\n
             * this method will help.
             *
             * \param id Resource ID
             * \param filename The filename of the resource
             * \param param2 Is a parameter given by the type of the template
             * \return A boolean if loading was successful
             */
            template <typename Parameter>
            bool load(Identifier id, const std::string &filename, const Parameter &param2);

            /** \brief Get a resource
             *
             * Get the resource whose ID you give as parameter.\n
             * Only use this method if you've already loaded a resource for the ID.
             *
             * \param id ID of the resource to get
             * \return The resource you want
             */
            Resource *get(Identifier id);

            /** \brief Get a resource
             *
             * Get the resource whose ID you give as parameter.\n
             * Only use this method if you've already loaded a resource for the ID.
             *
             * \param id ID of the resource to get
             * \return The resource you want
             */
            const Resource *get(Identifier id) const;

        private:
            bool insertResource(Identifier id, Resource *res);
            Resource *getResource(Identifier id);

            std::map<Identifier, Resource*> resMap;
    };

    #include "ResourceManager.inl"
}

#endif // SE_RECOURCEMANAGER_HPP
