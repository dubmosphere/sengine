#ifndef SE_GAME_HPP
#define SE_GAME_HPP

// SFML includes
#include <SFML/Graphics/RenderWindow.hpp>

// STL includes
#include <stack>

/** \mainpage Welcome to SEngine
 *
 * Hello,\n
 * \n
 * this is a little game framework for SFML. All you need is\n
 * a static SFML 2.2 and Codeblocks 13.12 with GCC 4.9 on Linux and MinGW / MinGW-w64 4.9 on Windows.\n
 * (Maybe it would work in VS too, i did not test it)\n
 * \n
 * The documentation is not finished yet, i am expanding it from time to time.\n
 * \n
 * BUILD INSTRUCTIONS:\n
 * -Open the codeblocks project\n
 * -Make sure you have all the libraries needed in the setup for your platform\n
 * -If you have the libraries installed, check the search directories of the project setup you want to use\n
 * -Press on build\n
 * \n
 * USAGE (Codeblocks):\n
 * -To use SE you just need to put the static library file into your library folder\n
 * -You will need the include folder too, so put it somewhere you think you need\n
 * -Linux: For example /usr/lib and the include folder to /usr/include\n
 * -Windows: Add the path of the lib folder and include folder to your search directories in codeblocks\n
 * \n
 * Have fun\n
 * Dubmosphere
 */
namespace se
{
    class GameState;

    /** \brief Game class for running the game...
     *
     * This class contains the method to start the game and to set the current gamestate.\n
     * Inherit from this class to create own game classes. Refer to your own gameclass in gamestates,\n
     * if you do that. Else you can refer to this class.
     */
    class Game
    {
        public:
            /** \brief Constructor
             *
             * Constructor
             */
            Game();
            ~Game();

            void createWindow(sf::VideoMode mode, const sf::String &title, sf::Uint32 style = sf::Style::Default, const sf::ContextSettings &settings = sf::ContextSettings());

            sf::RenderWindow &getWindow();

            /** \brief Run the game
             *
             * This method runs the game and starts the gameloop.\n
             * At the end it returns the exitcode of the program.
             *
             * \return Returns the exitcode of the app
             */
            int run();

            void pushState(GameState *gameState);
            void popState();
            void changeState(GameState *gameState);
            GameState *peekState();

            const float FPS; /**< Frames per second */
            const sf::Time TPF; /**< Time per frame */
        private:
            void updateLoop(sf::Time &accumulator, sf::Clock &clock);
            void gameLoop();

            sf::RenderWindow window;
            std::stack<se::GameState*> gameStates;
    };
}


#endif // SE_GAME_HPP
