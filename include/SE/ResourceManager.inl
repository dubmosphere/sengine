template <typename Resource, typename Identifier>
ResourceManager<Resource, Identifier>::~ResourceManager()
{
    // Simpler: auto it
    if(!resMap.empty()) {
        for(typename std::map<Identifier, Resource*>::iterator it = resMap.begin(); it != resMap.end();)
        {
            delete it->second;
            resMap.erase(it++);
        }

        resMap.clear();
    }
}

template <typename Resource, typename Identifier>
bool ResourceManager<Resource, Identifier>::load(Identifier id, const std::string &filename)
{
    Resource *res = new Resource;
    if(!res->loadFromFile(filename))
        throw std::runtime_error("ResourceManager::load - Failed to load " + filename);

    return insertResource(id, res);
}

template <typename Resource, typename Identifier>
template <typename Parameter>
bool ResourceManager<Resource, Identifier>::load(Identifier id, const std::string &filename, const Parameter &param2)
{
    Resource *res = new Resource;
    if(!res->loadFromFile(filename, param2))
        throw std::runtime_error("ResourceManager::load - Failed to load " + filename);

    return insertResource(id, res);
}

template <typename Resource, typename Identifier>
Resource *ResourceManager<Resource, Identifier>::get(Identifier id)
{
    return getResource(id);
}

template <typename Resource, typename Identifier>
const Resource *ResourceManager<Resource, Identifier>::get(Identifier id) const
{
    return getResource(id);
}

// Private member Methods
template <typename Resource, typename Identifier>
bool ResourceManager<Resource, Identifier>::insertResource(Identifier id, Resource *res)
{
    // Simpler: auto inserted, but this is faster
    std::pair<std::_Rb_tree_iterator<std::pair<const Identifier, Resource*>>, bool> inserted = resMap.insert(std::make_pair(id, res));
    return inserted.second;
}

template <typename Resource, typename Identifier>
Resource *ResourceManager<Resource, Identifier>::getResource(Identifier id)
{
    return resMap.at(id);
}
