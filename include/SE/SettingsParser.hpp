#ifndef SETTINGSPARSER_HPP
#define SETTINGSPARSER_HPP

// STL includes
#include <string>
#include <map>
#include <vector>
#include <sstream>

namespace se {
    class SettingsParser
    {
        public:
            SettingsParser();
            ~SettingsParser();

            bool loadFromFile(const std::string& filename);
            bool saveToFile();

            bool isChanged() const;

            template<typename T>
            void get(const std::string& key, T & value) const;
            template<typename T>
            void get(const std::string& key, std::vector<T> &value) const;

            template<typename T>
            void set(const std::string &key, const T value);
            template<typename T>
            void set(const std::string& key, const std::vector<T> value);

            void print() const;

        private:

            //return the string in the type of T
            template<typename T>
            T convertToType(const std::string &input) const;
            //return string of type T
            template<typename T>
            std::string convertToStr(const T input) const;

            bool read();
            bool write() const;
            std::pair<std::string, std::string> parseLine(const std::string &line) const;

            bool m_isChanged;
            std::string m_filename;
            std::map<std::string, std::string> m_data;
            const std::locale m_locale;
    };

    #include "SettingsParser.inl"

}

#endif // SETTINGSPARSER_HPP
