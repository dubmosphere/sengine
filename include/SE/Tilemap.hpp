#ifndef SE_TILEMAP_HPP
#define SE_TILEMAP_HPP

// SFML includes
#include <SFML/Graphics.hpp>

namespace se {
    struct Tile {
        int tileNum = 0;
        bool solid = false;
        sf::Vector2f pos = sf::Vector2f(0, 0);
    };

    class Tilemap : public sf::Drawable, public sf::Transformable {
        public:
            Tilemap();

            bool load(sf::String tileSet, sf::Vector2i mapSize, sf::Vector2i tileSize, int *tiles);
            bool load(std::string fileMap);
            sf::Vector2i getMapSize();
            sf::Vector2i getTileSize();
            Tile getTile(int x,int y);

        private:
            virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

            sf::Texture tileSet;
            sf::VertexArray vertices;
            sf::Vector2i mapSize;
            sf::Vector2i tileSize;
            std::vector<std::vector<Tile>> tiles;
    };
}

#endif // SE_TILEMAP_HPP
