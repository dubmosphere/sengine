#ifndef LUNA_HPP
#define LUNA_HPP

// Lua includes
#include <lua.hpp>

namespace se {
    /** \brief Class for helping using C++ Objects in Lua
     *
     * This class is originally written by nornagon.\n
     * His class was inspired by Lenny Palozzis one for older Lua versions\n
     * I just separated the definitions of the declarations and put it into\n
     * my engines namespace for nicer looking integration.\n
     * \n
     * Usage:\n
     * class Foo {\n
     *     int foo(lua_State *L) {\n
     *         cout << "Hello" << endl;\n
     *         return 0;\n
     *     }\n
     *     static const char className[];\n
     *     static const Luna<Foo>::RegType reg[];\n
     * };\n
     * const char Foo::className[] = "Foo";\n
     * const Luna<Foo>::RegType Foo::reg[] = {\n
     *     {"foo", &Foo::foo}\n
     *     {0}\n
     * };\n
     * \n
     * Luna<Foo>::reg(L);\n
     * \n
     * Copyright (c) 2014, nornagon\n
     * All rights reserved.\n
     * \n
     * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:\n
     *
     * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.\n
     * \n
     * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer\n
     *    in the documentation and/or other materials provided with the distribution.\n
     * \n
     * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,\n
     * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.\n
     * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,\n
     * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;\n
     * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,\n
     * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
     */
    template <class T>
    class Luna {
        public:
            struct RegType {
                const char *name;
                int(T::*mfunc)(lua_State*);
            };

            static void reg(lua_State *L);

        private:
            static int constructor(lua_State *L);
            static int thunk(lua_State *L);
            static int destructor(lua_State *L);

        protected:
            Luna();
    };

    #include "Luna.inl"
}

#endif // LUNA_HPP
