#ifndef SE_LOG_HPP
#define SE_LOG_HPP

// STL includes
#include <fstream>

namespace se
{
    class Log
    {
        public:
            enum LogType {
                INFO, ERROR, DEBUG
            };

            ~Log();

            static bool write(LogType type, std::string message);

        private:
            Log();

            static std::ofstream logStream;
    };
}

#endif // SE_LOG_HPP
